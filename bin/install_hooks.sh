#!/bin/sh
# Base Git hook install script.
#
# This script can be used to setup the required Git hooks in the local Git repo.

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
PROJECT_DIR="$(cd "$DIR/.." > /dev/null 2>&1 && pwd)"
GIT_HOOK_DIR="$( cd "$PROJECT_DIR/.git/hooks/" > /dev/null 2>&1 && pwd )"
TEMPLATE_DIR="$( cd "$PROJECT_DIR/git-hooks" > /dev/null 2>&1 && pwd )"
HOOK_NAMES="pre-commit pre-push"


echo "Installing Git hooks..."
for hook in $HOOK_NAMES; do
  # If there is a hook in the TEMPLATE_DIR then install it
  if [ -f $TEMPLATE_DIR/$hook ]; then
    echo "> Hook $hook"
    if [ ! -h $GIT_HOOK_DIR/$hook -a -h $GIT_HOOK_DIR/$hook ]; then
      # If the hook already exists, is executable, and is not a symlink
      # then disable it before installing our hook.
      echo "> old git hook $hook disabled"
      mv $GIT_HOOK_DIR/$hook $GIT_HOOK_DIR/$hook.local
    fi

    echo "> Enable project git hook"
    ln -s -f $TEMPLATE_DIR/$hook $GIT_HOOK_DIR/$hook
  fi
done
