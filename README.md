# Git-Hooks

This repo holds the git hooks we use to run rubocop on all changed files in a
pre-commit hook and rspec on all files in a pre-push hook.

It also includes a shell script that can be used to setup the Git hooks for the
first time in a project.

# Install

To install the git hooks, copy the `bin/install_hooks.sh` and `git-hooks/` folder
to the target project.  You should then be able to run the
`bin/install_hooks.sh` script in your project in order to copy the hook files to
the appropriate location in `.git/hooks`

# Uninstall
To remove the hooks, delete the files `.git/hooks/pre-commit` and
`.git/hooks/pre-push` from your project directory.
